package tl.knyt.waterrower

import java.io.File
import java.time.Instant
import java.time.LocalDateTime

fun openTempGpx(): File {
    val file = File("/tmp/export-${LocalDateTime.now()}.gpx")
    file.writeText(header)
    return file
}

fun File.writeFooter() {
    appendText(footer)
}

fun File.appendTrkpt(distance: Double, cadence: Int, heartRate: Int, latitude: Double, longitude: Double) {
    appendText(
        """
        <trkpt lat="${"%.8f".format(latitude)}" lon="${"%.8f".format(longitude)}">
            <time>${Instant.now()}</time>
            <extensions>
                <gpxtpx:TrackPointExtension>
                    ${""/*TODO <gpxtpx:hr>$heartRate</gpxtpx:hr>*/}
                    <gpxtpx:cad>$cadence</gpxtpx:cad>
                </gpxtpx:TrackPointExtension>
            </extensions>
        </trkpt>

        """.trimIndent()
    )
}

private val header: String =
    """
    <?xml version="1.0" encoding="UTF-8"?>
    <gpx>
    <trk>
        <name>WaterRower rowing</name>
        <trkseg>

    """.trimIndent()

private val footer: String =
    """
        </trkseg>
      </trk>
    </gpx>

    """.trimIndent()
