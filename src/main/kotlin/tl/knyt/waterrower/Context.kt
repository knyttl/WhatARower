package tl.knyt.waterrower

import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.Properties

fun contextOf(propertyFile: String): Context =
    contextOf(
        propertyFile,
        Properties().apply {
            try {
                load(FileInputStream(propertyFile))
            } catch (e: IOException) {
                error("You need to create '.properties` file and put there 'strava.clientId' and 'strava.clientSecret' values.")
            }
        }
    )

fun contextOf(propertyFile: String, properties: Properties): Context =
    Context(
        propertyFile,
        properties,
        properties.get("strava.clientId") as String? ?: error("Missing strava.clientId property in '.properties' file."),
        properties.get("strava.clientSecret") as String? ?: error("Missing strava.clientSecret property in '.properties' file."),
        // TODD read from auth
        stravaAthleteId = properties.get("strava.athleteId")?.toString()?.toLong() ?: 5616663L,
        stravaRefreshToken = properties.get("strava.refreshToken") as String?
    )

data class Context(
    val propertyFile: String,
    val properties: Properties,
    val stravaClientId: String,
    val stravaClientSecret: String,
    // TODD read from auth
    val stravaAthleteId: Long?,
    var stravaAccessToken: String? = null,
    var stravaRefreshToken: String? = null
) {
    fun store() {
        properties.put("strava.athleteId", stravaAthleteId.toString())
        properties.put("strava.refreshToken", stravaRefreshToken.toString())
        properties.store(FileOutputStream(propertyFile), "")
    }
}
