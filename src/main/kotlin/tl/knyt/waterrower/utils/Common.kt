package tl.knyt.waterrower.utils

import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.gson.responseObject
import org.apache.logging.log4j.LogManager

val log = LogManager.getLogger()

inline fun <reified T : Any> Request.fetch() = responseObject<T>()
    .third
    .run {
        component2()?.also { error(it) }
        component1()!!
    }

fun String.openUrl() {
    val rt = Runtime.getRuntime()
    rt.exec("open " + this)
}
