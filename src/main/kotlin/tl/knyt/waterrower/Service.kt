package tl.knyt.waterrower

import tl.knyt.waterrower.services.Strava
import tl.knyt.waterrower.services.findPointForDistance
import tl.knyt.waterrower.services.s4.connectToS4
import tl.knyt.waterrower.services.toPath
import tl.knyt.waterrower.utils.log
import tl.knyt.waterrower.utils.openUrl

fun main() {
    val context = contextOf(".properties")
    val rower = connectToS4()
    val strava = Strava(context.stravaClientId, context.stravaClientSecret, context.stravaAthleteId, context.stravaRefreshToken)

    if (context.stravaRefreshToken.isNullOrBlank()) {
        stravaAuth(strava, context)
    } else {
        stravaRefresh(strava, context)
    }

    // fetch routes which are shorter than 20km
    val routes = strava.routes().filter { it.distance < 20_000 }

    println("Found Strava routes:")
    routes.forEachIndexed { index, route -> println("\t%2d%15s\t%5d m".format(index + 1, route.name, route.distance.toLong())) }

    println("\nChoose one of the routes to row: ")
    val routeIndex = readLine()?.toIntOrNull() ?: error("Invalid route index given.")
    val route = routes[routeIndex - 1]

    println("\nChosen route: ${route.name}/${route.id}")
    val path = strava.route(route.id).toPath()
    val gpx = openTempGpx()

    rower.subscribe(
        onPulse = { distance, cadence ->
            log.info("Distance: $distance, Cadence: $cadence")
            val point = findPointForDistance(path, distance)
            gpx.appendTrkpt(distance, cadence, 125, point.lat, point.lon)
        },
        onCleanUp = {
            gpx.writeFooter()
            strava
                .upload(gpx)
                .also { response -> log.info(response) }
        }
    )

    rower.connect()
}

fun stravaAuth(strava: Strava, context: Context) {
    strava.oAuthUrl("read_all,activity:write").openUrl()
    println("Paste the response URL from your brorser:")
    val authUrl = readLine()
    if (authUrl.isNullOrBlank()) error("Invalid auth code - was null or empty.")
    val (authCode) = "code=([a-f0-9]+)".toRegex().find(authUrl)!!.destructured
    strava.handleCode(authCode)
    context.stravaRefreshToken = strava.refreshToken
    context.store()
}

fun stravaRefresh(strava: Strava, context: Context) {
    strava.refreshToken = context.stravaRefreshToken
    strava.refreshToken()
    context.stravaRefreshToken = strava.refreshToken
    context.store()
}
