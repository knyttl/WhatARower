package tl.knyt.waterrower.services.s4

import com.fazecast.jSerialComm.SerialPort
import de.tbressler.waterrower.IWaterRowerConnectionListener
import de.tbressler.waterrower.WaterRower
import de.tbressler.waterrower.WaterRowerInitializer
import de.tbressler.waterrower.io.msg.AbstractMessage
import de.tbressler.waterrower.io.msg.`in`.DecodeErrorMessage
import de.tbressler.waterrower.io.msg.`in`.PingMessage
import de.tbressler.waterrower.io.msg.`in`.PulseCountMessage
import de.tbressler.waterrower.io.msg.`in`.StrokeMessage
import de.tbressler.waterrower.io.transport.JSerialCommDeviceAddress
import de.tbressler.waterrower.model.ErrorCode
import de.tbressler.waterrower.model.ModelInformation
import de.tbressler.waterrower.model.StrokeType
import de.tbressler.waterrower.subscriptions.DistanceSubscription
import de.tbressler.waterrower.subscriptions.ISubscription
import tl.knyt.waterrower.utils.log
import java.time.Duration
import java.time.Instant

fun connectToS4(): S4Listener {
    val initializer = WaterRowerInitializer(Duration.ofSeconds(2), Duration.ofSeconds(2), 5)
    val waterRower = WaterRower(initializer)
    val ports = availablePorts()
    if (ports.isEmpty()) {
        error("No WaterRower connection found.")
    }

    log.info("Available ports: ${ports.map { it.value() }}")
    return S4Listener(waterRower, ports)
}

/* Updates the available serial ports on the stack. */
private fun availablePorts(): List<JSerialCommDeviceAddress> = SerialPort.getCommPorts()
    .toList()
    .onEach { println(it.descriptivePortName + "/" + it.systemPortName) }
    .filterNot { it.systemPortName.contains("Bluetooth") || it.systemPortName.contains("BT") || it.systemPortName.contains("Phone") }
    .filterNot { it.systemPortName.startsWith("/dev/cu.") }
    .filter {
        if (it.isOpen) {
            log.info("Skipping $it as it aleady open.")
        }
        !it.isOpen
    }
    .map { JSerialCommDeviceAddress(it.systemPortName) }

class S4Listener(private val waterRower: WaterRower, private val ports: List<JSerialCommDeviceAddress>) {
    private var pulses: Int = 0
    private var strokes: Int = 0
    private var last: Instant = Instant.now()
    private val pulseToMeterCoef: Double = 1.1 / 100
    private val strokeCounter = PulseStack()

    fun subscribe(onPulse: (pulses: Double, cadence: Int) -> Unit, onCleanUp: () -> Unit) {
        waterRower.addConnectionListener(object : IWaterRowerConnectionListener {
            override fun onConnected(modelInformation: ModelInformation?) {
                log.info("Connected to " + modelInformation)
            }

            override fun onDisconnected() {}
            override fun onError(errorCode: ErrorCode?) {
                log.info(errorCode)
            }
        })

        waterRower.subscribe(object : DistanceSubscription(DistanceMode.DISPLAYED_DISTANCE) {
            override fun onDistanceUpdated(mode: DistanceMode, distance: Int) {
                log.debug("Distance: $distance")
            }
        })

        waterRower.subscribe(object : ISubscription {
            override fun handle(msg: AbstractMessage) {
                when (msg) {
                    is PulseCountMessage -> {
                        pulses += msg.pulsesCounted
                        if (last.epochSecond < Instant.now().epochSecond) {
                            onPulse(pulses * pulseToMeterCoef, strokeCounter.sum() * strokeCounter.minuteFraction())
                            last = Instant.now()
                        }
                    }
                    is StrokeMessage -> {
                        log.info("Stroke: ${msg.strokeType}")
                        if (msg.strokeType == StrokeType.END_OF_STROKE) {
                            strokes++
                            strokeCounter.pulse()
                        }
                    }
                    is DecodeErrorMessage -> {
                        waterRower.disconnect()
                        onCleanUp()
                    }
                    is PingMessage -> {
                    }
                    else -> log.debug(msg)
                }
            }

            override fun poll() = null
        })
    }

    fun connect() {
        waterRower.connect(ports.first())
    }
}
