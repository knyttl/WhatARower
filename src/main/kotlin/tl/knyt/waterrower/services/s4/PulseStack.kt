package tl.knyt.waterrower.services.s4

import java.time.Instant
import java.util.LinkedList

class PulseStack(val period: Int = 15) : LinkedList<Pulse>() {

    fun pulse(pulses: Int = 1): Boolean {
        clean()
        return super.add(Pulse(now(), pulses))
    }

    fun sum(): Int {
        clean()
        return sumBy { it.pulses }
    }

    private fun clean() {
        while (isNotEmpty() && super.peekFirst().timestamp < now() - period) {
            super.pollFirst()
        }
    }

    fun minuteFraction() = 60 / period

    companion object {
        private fun now() = Instant.now().epochSecond
    }
}

data class Pulse(val timestamp: Long, val pulses: Int)
