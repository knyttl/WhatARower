package tl.knyt.waterrower.services

import com.github.kittinunf.fuel.core.FileDataPart
import com.github.kittinunf.fuel.core.InlineDataPart
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.fuel.httpPost
import com.github.kittinunf.fuel.httpUpload
import java.io.File
import kotlinx.serialization.Serializable
import tl.knyt.waterrower.utils.fetch

class Strava(
    val clientId: String,
    val clientSecret: String,
    var athleteId: Long? = null,
    var refreshToken: String? = null,
    private var accessToken: String? = null
) {
    // https://developers.strava.com/playground/
    /** this is an oauth start endpoint, user needs to voluntarily accept terms at strava site */
    fun oAuthUrl(scopes: String, redirectUrl: String = "http://localhost/") =
        "https://www.strava.com/oauth/authorize" +
            "?client_id=$clientId" +
            "&scope=$scopes" +
            "&response_type=code" +
            "&approval_prompt=auto&redirect_uri=$redirectUrl"

    /** once user accepts strava terms, he is given a code, which is transformed into accessTokens here */
    fun handleCode(code: String): AuthResponse =
        "https://www.strava.com/oauth/token"
            .httpPost(
                listOf(
                    "client_id" to clientId,
                    "client_secret" to clientSecret,
                    "code" to code
                )
            )
            .fetch<AuthResponse>()
            .also {
                accessToken = it.access_token
                refreshToken = it.refresh_token
            }

    /** before we use the accessToken, we need to refresh it */
    fun refreshToken(): AuthResponse =
        "https://www.strava.com/oauth/token"
            .httpPost(
                listOf(
                    "client_id" to clientId,
                    "client_secret" to clientSecret,
                    "grant_type" to "refresh_token",
                    "refresh_token" to refreshToken
                )
            )
            .fetch<AuthResponse>()
            .also {
                accessToken = it.access_token
                refreshToken = it.refresh_token
            }

    fun routes(): List<RoutesResponse> =
        "https://www.strava.com/api/v3/athletes/$athleteId/routes"
            .httpGet()
            .header("Authorization" to "Bearer $accessToken")
            .fetch<List<RoutesResponse>>()

    fun route(id: Long): String =
        "https://www.strava.com/api/v3/routes/$id/export_gpx"
            .httpGet()
            .header("Authorization" to "Bearer $accessToken")
            .responseString()
            .third
            .component1()!!

    // https://developers.strava.com/docs/uploads/
    fun upload(file: File) = "https://www.strava.com/api/v3/uploads".httpUpload(method = Method.POST)
        .add(FileDataPart(name = "file", file = file))
        .add(InlineDataPart(name = "name", content = "Rowing"))
        .add(InlineDataPart(name = "data_type", content = "gpx"))
        .add(InlineDataPart(name = "activity_type", content = "Rowing"))
        .header("Authorization", "Bearer $accessToken")
        .response()
}

@Serializable
data class AuthResponse(
    val token_type: String,
    val expires_in: Int?,
    val expires_at: Int,
    val refresh_token: String,
    val access_token: String
)

@Serializable
data class RoutesResponse(
    val id: Long,
    val name: String,
    val distance: Double
)
