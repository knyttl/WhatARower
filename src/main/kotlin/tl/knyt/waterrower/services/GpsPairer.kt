package tl.knyt.waterrower.services

import kotlin.math.absoluteValue
import kotlin.streams.toList
import org.apache.lucene.util.SloppyMath

fun String.toPath() =
    this
        .split("\n")
        .filter { it.contains("lat=") && it.contains("lon=") }
        .map { Point(it) }
        .toList()
        .zipWithNext { p1, p2 -> Edge(p1, p2) }

data class Point(val lat: Double, val lon: Double) {
    companion object {
        fun matcher(attribute: String) = "$attribute=\"(.*?)\"".toRegex()
        fun Regex.matchPosition(node: String): Double = this.find(node)!!.groupValues[1].toDouble()
    }

    constructor(node: String) : this(
        matcher("lat").matchPosition(node),
        matcher("lon").matchPosition(node)
    )
}

data class Edge(
    val p1: Point,
    val p2: Point,
    val length: Double = SloppyMath.haversinMeters(p1.lat, p1.lon, p2.lat, p2.lon).absoluteValue
)

fun findPointForDistance(path: List<Edge>, distance: Double): Point {
    var restDistance = distance
    for (edge in path) {
        if (restDistance - edge.length < 0) {
            val ratio = restDistance / edge.length
            return Point(edge.p1.lat + (edge.p2.lat - edge.p1.lat) * ratio, edge.p1.lon + (edge.p2.lon - edge.p1.lon) * ratio)
        }
        restDistance -= edge.length
    }
    error("Distance over GPX track.")
}
