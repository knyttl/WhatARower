package tl.knyt.waterrower

import java.time.Instant
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import tl.knyt.waterrower.services.s4.Pulse
import tl.knyt.waterrower.services.s4.PulseStack

internal class PulseStackTest {

    @Test
    fun testPulsesOld() {
        val ps = PulseStack(period = 15)
        ps.add(Pulse(Instant.now().epochSecond - 63, 1))
        ps.add(Pulse(Instant.now().epochSecond - 42, 1))
        ps.add(Pulse(Instant.now().epochSecond - 21, 1))
        ps.add(Pulse(Instant.now().epochSecond - 12, 1))
        ps.add(Pulse(Instant.now().epochSecond - 5, 1))

        assertEquals(2, ps.sum())
        assertEquals(4, ps.minuteFraction())
    }
}
